import netCDF4 as nc
import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
import tensorflow as tf


def plot_loss(history, name):
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    # plt.ylim([0, 10])
    plt.xlabel('Epoch')
    plt.ylabel('Error [MPG]')
    plt.legend()
    plt.grid(True)
    plt.savefig(name + ".png")


def getModel(temporalLength):
    model = tf.keras.models.Sequential([
        tf.keras.layers.Normalization(),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Conv3D(64,[3,3,int(temporalLength/2)],input_shape=(10,10,temporalLength,1) , activation = 'sigmoid'),
        tf.keras.layers.Conv3D(32,[3,3,1],activation = 'sigmoid'),
        # tf.keras.layers.Conv3D(16,[3,3,1],activation = 'relu'),

        tf.keras.layers.Normalization(),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(256*4, activation='relu'),
        tf.keras.layers.Dense(256*3, activation='relu'),
        tf.keras.layers.Dense(256*2, activation='relu'),
        tf.keras.layers.Dense(256*1, activation='relu'),
        tf.keras.layers.Dense(1)
    ])

    model.compile(loss='mean_absolute_error', optimizer=tf.keras.optimizers.Adam(0.001))
    return model


def getData(name):
    data = nc.Dataset(name)
    return data


def predict(model, data, name, temporalLength):
    testData = []
    # data.variables['tempanomaly'][-12,5,5] = 0
    for month in range(len(data.variables['tempanomaly']) -(12+temporalLength), len(data.variables['tempanomaly'])- temporalLength):
        temp = np.ndarray([10,10,temporalLength])

        for time in range(temporalLength):
            temp[:,:,time] = data.variables['tempanomaly'][month+time]
        temp[-(month+1):,4:7,4:7] = 0
        testData.append(temp)

    results = model.predict(tf.reshape(tf.convert_to_tensor(np.asarray(testData)), (12,10,10,temporalLength,1)))
    print(results)
    d = {"Predictions": np.asarray(results)[:,0]}
    df = pd.DataFrame(data=d)
    df.to_csv(name + ".csv")


def trainModel(data, model, temporalLength, plot, name):
    # preprocess data

    trainingData = []
    targets = []
    for month in range(len(data.variables['tempanomaly'][:-temporalLength*2])):
        temp = np.ndarray([10,10,temporalLength])

        for time in range(temporalLength):
            temp[:,:,time] = data.variables['tempanomaly'][month+time]
        temp[-1,4:7,4:7] = 0
        trainingData.append(temp)
        targets.append(float(data.variables['tempanomaly'][month+temporalLength,5,5]))

    print(np.asarray(trainingData).shape)
    np.random.seed(69)
    shuffledTrainData = np.random.shuffle(np.asarray(trainingData))
    np.random.seed(69)
    shuffledTargets = np.random.shuffle(np.asarray(targets))
    history = model.fit(
        tf.reshape(tf.convert_to_tensor(np.asarray(trainingData)), (np.asarray(trainingData).shape[0], 10, 10, temporalLength, 1)),
        tf.convert_to_tensor(np.asarray(targets)),
        # validation_split = 0.2,
        verbose=1,
        epochs=50,
        batch_size=32
    )

    

    # if plot:
    #     plot_loss(history, name)

    return model


def main():
    temporalLength = 100
    name = "ex3_CNN"
    data = getData("data/temp_anomaly_ex3.nc")

    # print(data.variables['tempanomaly'][1419,4:7,4:7])
    # exit()
    model = getModel(temporalLength)
    model = trainModel(data, model, temporalLength, True, name)
    model.save("Models/" + name)

    model = tf.keras.models.load_model("Models/" + name, compile=False)
    predict(model,data,name, temporalLength)

    return 0


if __name__ == "__main__":
    main()
